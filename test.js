
var result = document.getElementById("result");

function hitNum1() {
    var nums = [1,2,5,4,3];
    var numberResult = 0;
    for (var index = 0; index < nums.length; index++) {
        var check = true;    
        var x = nums[index];
        for (var i = 0; i < nums.length; i++) {
            var j = x - nums[i];
            if (j < 0) {
                check = false;
            }
        }
        if (check) {
            // result.innerText = x;
            numberResult = x;
        }
        //alert(check);
    }

    result.innerText = "nums =  "+nums+"\n"+"Result : "+numberResult;
    

}

function hitNum2() {
    var data = [1,2,5,4,3,8,9];
    var nums = [];
    //data.push(nums);
    var x = 3;

    for (var index = 0; index < data.length; index++) {
        var check = true;
        for (var i = 0; i < data.length; i++) {
            var divide = data[index] / data[i];
            if (divide == x) {
                check = false;
            }
        }

        if (check) {
            nums.push(data[index]);
        }
    }

    result.innerText = "nums =  "+data+"\n"+"x = "+x+"\n"+"Result : "+nums;
}

function hitNum3(){
    var word = "lorem ipsum doler amet";
    var x = 5;
    var res = word.split(" ");

    for (let index = 0; index < res.length; index++) {
        if (res[index].length != 5) {
            res.splice(index,1);
        }
    }

    result.innerText = "word =  "+word+"\n"+"x = "+x+"\n"+"Result : "+res;
}